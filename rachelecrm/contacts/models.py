from __future__ import unicode_literals

from django.db import models

from shortuuidfield import ShortUUIDField


class Contact(models.Model):
	uuid = ShortUUIDField(unique=True)
	first_name = models.CharField(max_length=20)
	last_name = models.CharField(max_length=20)
